---
layout: job_family_page
title: "Sr. Solutions Manager"
---

The Senior Solutions Manager role is a leadership role balancing field delivery with operations and strategy. This is a player/coach role where the individual is expected to be experienced in and have advanced insight into the GitLab platform. The Senior Solutions Manager will work together with the other managers within the Customer Success organization to help execute on strategies and vision with the VP, Customer Success.

The Senior Solutions Manager is a critical part of the Customer Success organization at GitLab. Leading the charge for increased adoption and usage at our premier customers, the Senior Solutions Manager will design, implement and support the Professional Services organization in the delivery of new and existing services, including piloting our Embedded Engineering program.

You will have the opportunity to help shape and execute a strategy to help the Services team build mindshare and broad use of the GitLab platform within enterprise customers. Coaching team members to becoming the trusted advisors to their customers, the embedded engineers will be able to support best practices, GitLab adoption and support of strategic customer’s goals with GitLab as a tool. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting and management. You should also have a demonstrated ability to think strategically about business, products, and technical challenges. You will also be responsible for helping grow and maintain our enterprise-level customers.

## Responsibilities

- Work with the VP, Customer Success to help establish strategic and operational practices (e.g., people, skill sets, engagement approaches, processes and policies, enablement and coaching) for new services such as Embedded Engineering 
- Develop strategies and drive plans to evaluate and build new service offerings (i.e., product-level, operational best practices, and training and education), including service definition (e.g., scope, pricing, etc.), collateral and sales enablement
- Share hands-on technical preparation and presentation work for key accounts helping sell on the value of what GitLab professional services has to offer
- Be responsible for successfully introducing and incubating new services like the Embedded Engineering program to deliver on key success measures, including customer-facing engagements and internal initiatives
- Monitor performance of the program and provide timely feedback, facilitate development assistance and drive other internal initiatives as needed
- Partner with the sales teams to build services forecasts for new and existing services
- Help to manage resource assignments and staffing levels, including recruitment as needed
- Identify and implement improvements to the processes and tools used as a seasoned with experience leading teams of project managers and consultants in support of external customers
- Provide leadership and guidance to coach, motivate and lead embedded engineers members to their optimum performance levels 
- Ensure delivery model is focused on quality, cost-effective delivery of services, and customer success outcomes
- Remains knowledgeable and up-to-date on GitLab releases
- Documents services provided to customers, including new code, techniques, and processes, in such a way as to make such services more efficient in the future and to add to the GitLab community
- Works with the Product Development and Support teams to contribute documentation for GitLab

## Requirements

- Experienced in and with advanced insight to the GitLab platform
- Experienced in giving and receiving positive and constructive feedback
- Experienced in development of strategies, deliver on programs and definition of processes, policies and procedures
- Able to adapt to environmental change and retrospecting on success and failures
- Previous leadership experience is a plus
- Experienced in collaborating with other managers and executing strategies
- Proven track record in software/technology sales or consulting and management
- Demonstrated ability to think strategically about business, products, and technical challenges

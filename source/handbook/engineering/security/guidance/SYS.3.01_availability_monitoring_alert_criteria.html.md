---
layout: markdown_page
title: "SYS.3.01 - Availability Monitoring Alert Criteria Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SYS.3.01 - Availability Monitoring Alert Criteria

## Control Statement

GitLab defines availability monitoring alert criteria, how alert criteria will be flagged, and identifies authorized personnel for flagged system alerts.

## Context

In order for alerts to be configured for availability, we first have to establish what criteria we use for altering. This control simply formalizes the need for GitLab to explicitly define what criteria we use for availability alerting.

## Scope

This control applies to all production systems related to the GitLab SaaS product.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.3.01_availability_monitoring_alert_criteria.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.3.01_availability_monitoring_alert_criteria.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SYS.3.01_availability_monitoring_alert_criteria.md).

## Framework Mapping

* ISO
  * A.12.1.3
  * A.17.2.1
* SOC2 CC
  * CC7.2
* SOC2 Availability
  * A1.1

---
layout: markdown_page
title: Macros
category: Zendesk
---


We use Zendesk Macros as a tool to help support common workflows and allow us to support our customers faster. It's important to find a balance of when and where to use a macro as too many macro responses and the support process becomes robotic and canned. Nobody likes cans.

## Who can create a Macro?

Macros can be created by agents, engineers, and managers in Zendesk. They should be used for matters that require a rigid process (2FA re-auth/DMCA) or when we _need_ to provide a template response (Holiday Coverage).

## Where Macros Live

Macros are namespaced with a `::` which nests them in the bottom of Zendesk Dropdown. The current top level headings are below:

* Incident - Responses for users affected by incidents
* Escalation  - Used to move tickets between queues.
* General - General Cannery
* DMCA - Responses for the DMCA process
* Twitter - Used by the Community Advocacy team
* Security - Related to security questions
* Account - For resetting 2FA, GPDR, and dormant namespace requests
* Unpaid user - Responses for users on Gitlab.com `Free` or Self-Managed `Core`
* GitHost - Responses specific to GitHost inquiries
* GitLab.com - Responses specific to GitLab.com inquiries




